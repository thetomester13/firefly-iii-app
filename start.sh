#!/bin/bash

set -eu

mkdir -p /run/firefly-iii/framework-cache /run/firefly-iii/logs

if [[ ! -d /app/data/storage ]]; then
    echo "==> Creating storage directory"
    mkdir /app/data/storage
    cp -r /app/code/storage.original/* /app/data/storage
fi

# this can be removed in next release
rm -rf /app/data/storage/storage.original

if [[ ! -f /app/data/env ]]; then
    cp /app/pkg/env.production /app/data/env
fi

if [[ ! -f /app/data/.initialized ]]; then
    # https://docs.firefly-iii.org/installation/self_hosted
    echo "==> Initializing database on first run"
    php artisan migrate:refresh --seed
    php artisan firefly-iii:upgrade-database
    php artisan passport:install

    # Create a random APP_KEY and set it in our env file
    php artisan key:generate

    # disable update check -REPLACE https://github.com/firefly-iii/firefly-iii/issues/1050#issuecomment-352727173
    PGPASSWORD=${CLOUDRON_POSTGRESQL_PASSWORD} psql -h ${CLOUDRON_POSTGRESQL_HOST} -p ${CLOUDRON_POSTGRESQL_PORT} -U ${CLOUDRON_POSTGRESQL_USERNAME} -d ${CLOUDRON_POSTGRESQL_DATABASE} \
            -c "INSERT INTO configuration (name, data, created_at) VALUES ('permission_update_check', '0', '2020-01-21 20:00:00') ON CONFLICT (name) DO UPDATE SET data='0'"

    touch /app/data/.initialized
else
    # https://docs.firefly-iii.org/advanced-installation/upgrade
    # view:clear is not enough. otherwise we get some twig errors (https://github.com/firefly-iii/firefly-iii/issues/1936)
    rm -rf /app/data/storage/framework/views && mkdir -p /app/data/storage/framework/views/twig /app/data/storage/framework/views/v1

    echo "==> Upgrading database"
    php artisan migrate --seed
    php artisan firefly-iii:decrypt-all
    php artisan cache:clear
    php artisan firefly-iii:upgrade-database
    php artisan cache:clear
    php artisan view:clear
fi

# sessions, logs and cache 
rm -rf /app/data/storage/framework/cache && ln -s /run/firefly-iii/framework-cache /app/data/storage/framework/cache
rm -rf /app/data/storage/logs && ln -s /run/firefly-iii/logs /app/data/storage/logs

echo "==> Changing permissions"
chown -R www-data:www-data /app/data/ /run/firefly-iii/

echo "==> Staring Firefly III"

APACHE_CONFDIR="" source /etc/apache2/envvars
rm -f "${APACHE_PID_FILE}"
exec /usr/sbin/apache2 -DFOREGROUND
