FROM cloudron/base:2.0.0@sha256:f9fea80513aa7c92fe2e7bf3978b54c8ac5222f47a9a32a7f8833edf0eb5a4f4

ARG VERSION=5.4.6

RUN mkdir -p /app/code /app/pkg
WORKDIR /app/code

RUN apt update && \
    apt install -y language-pack-en language-pack-de language-pack-fr language-pack-nl language-pack-es language-pack-pt language-pack-ru -y && \
    apt install -y php7.4 php7.4-{bcmath,bz2,cgi,cli,common,curl,dba,dev,enchant,fpm,gd,gmp,imap,interbase,intl,json,ldap,mbstring,mysql,odbc,opcache,pgsql,phpdbg,pspell,readline,soap,sqlite3,sybase,tidy,xml,xmlrpc,xsl,zip} libapache2-mod-php7.4 php-{apcu,date,geoip,gettext,imagick,gnupg,pear,redis,twig,uuid,validate,zmq} && \
    apt remove -y php7.3 libapache2-mod-php7.3 && \
    locale-gen

# Install Firefly III
RUN composer create-project grumpydictator/firefly-iii --no-dev --prefer-dist . ${VERSION}

# Make a symlink for our storage directory and .env file to /app/data/ so we can write to it
RUN mv /app/code/storage /app/code/storage.original && \
    ln -s /app/data/storage /app/code/storage

RUN rm -f /app/code/.env && ln -sf /app/data/env /app/code/.env

# configure apache
RUN rm /etc/apache2/sites-enabled/*
RUN sed -e 's,^ErrorLog.*,ErrorLog "|/bin/cat",' -i /etc/apache2/apache2.conf
COPY apache/mpm_prefork.conf /etc/apache2/mods-available/mpm_prefork.conf
COPY apache/firefly-iii.conf /etc/apache2/sites-enabled/firefly-iii.conf
RUN echo "Listen 8000" > /etc/apache2/ports.conf
RUN echo "ServerName localhost" >> /etc/apache2/apache2.conf

RUN a2disconf other-vhosts-access-log
RUN a2enmod rewrite php7.4

RUN crudini --set /etc/php/7.4/apache2/php.ini PHP upload_max_filesize 64M && \
    crudini --set /etc/php/7.4/apache2/php.ini PHP post_max_size 64M && \
    crudini --set /etc/php/7.4/apache2/php.ini PHP memory_limit 64M && \
    crudini --set /etc/php/7.4/apache2/php.ini Session session.save_path /run/php/session && \
    crudini --set /etc/php/7.4/apache2/php.ini Session session.gc_probability 1 && \
    crudini --set /etc/php/7.4/apache2/php.ini Session session.gc_divisor 100

COPY start.sh env.production /app/pkg/

CMD [ "/app/pkg/start.sh" ]
