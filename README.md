# Firefly III Cloudron App

This repository contains the Cloudron app package source for [Firefly III](https://firefly-iii.org/).

## Installation

[![Install](https://cloudron.io/img/button.svg)](https://cloudron.io/button.html?app=org.fireflyiii.cloudronapp)

or using the [Cloudron command line tooling](https://cloudron.io/references/cli.html)

```
cloudron install --appstore-id org.fireflyiii.cloudronapp
```

## Building

The app package can be built using the [Cloudron command line tooling](https://cloudron.io/references/cli.html).

```
cd firefly-iii-app

cloudron build
cloudron install
```

## Notes

* Firefly III supports LDAP authentication

## Testing

The e2e tests are located in the `test/` folder and require [nodejs](http://nodejs.org/). They are creating a fresh build, install the app on your Cloudron, perform tests, backup, restore and test if things are ok.

```
cd firefly-iii-app/test

npm install
USERNAME=<cloudron username> PASSWORD=<cloudron password> npm test
```
