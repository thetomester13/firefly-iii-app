#!/usr/bin/env node

/* jslint node:true */
/* global it:false */
/* global xit:false */
/* global describe:false */
/* global before:false */
/* global after:false */

'use strict';

require('chromedriver');

var execSync = require('child_process').execSync,
    expect = require('expect.js'),
    path = require('path'),
    superagent = require('superagent'),
    webdriver = require('selenium-webdriver');

var by = webdriver.By,
    until = webdriver.until,
    Builder = require('selenium-webdriver').Builder;

process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

describe('Application life cycle test', function () {
    this.timeout(0);

    var server, browser = new Builder().forBrowser('chrome').build();
    var LOCATION = 'test';
    var app;
    var TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 5000;
    var username = process.env.USERNAME;
    var password = process.env.PASSWORD;

    before(function (done) {
        if (!process.env.USERNAME) return done(new Error('USERNAME env var not set'));
        if (!process.env.PASSWORD) return done(new Error('PASSWORD env var not set'));

        var seleniumJar= require('selenium-server-standalone-jar');
        var SeleniumServer = require('selenium-webdriver/remote').SeleniumServer;
        server = new SeleniumServer(seleniumJar.path, { port: 4444 });
        server.start();

        done();
    });

    after(function (done) {
        browser.quit();
        server.kill();
        done();
    });

    function userLogin(done) {
        browser.get('https://' + app.fqdn).then(function () {
            return browser.findElement(by.xpath('//input[@name="email"]')).sendKeys(username);
        }).then(function () {
            return browser.findElement(by.xpath('//input[@name="password"]')).sendKeys(password);
        }).then(function () {
            return browser.findElement(by.xpath('//button[@type="submit"]')).click();
        }).then(function () {
            return browser.sleep(5000);
        }).then(function () {
            return browser.wait(until.elementLocated(by.id('app')), TIMEOUT);
        }).then(function () {
            done();
        });
    }

    function cannotAdminLogin(done) {
        browser.get('https://' + app.fqdn).then(function () {
            return browser.findElement(by.xpath('//input[@name="email"]')).sendKeys('admin');
        }).then(function () {
            return browser.findElement(by.xpath('//input[@name="password"]')).sendKeys('password');
        }).then(function () {
            return browser.findElement(by.xpath('//button[@type="submit"]')).click();
        }).then(function () {
            return browser.findElement(by.xpath('//div[contains(@class, "alert")]')).getText();
        }).then(function(alertText) {
            if (!alertText.includes('These credentials do not match our records')) return Promise.reject(new Error('Wrong error message'));
        }).then(function () {
            done();
        });
    }

    function initializeAccount(done) {
        browser.get('https://' + app.fqdn).then(function () {
            return browser.findElement(by.id('ffInput_bank_name')).sendKeys('Cloudron Bank');
        }).then(function () {
            return browser.findElement(by.id('ffInput_bank_balance')).sendKeys('0');
        }).then(function () {
            return browser.findElement(by.xpath('//input[@type="submit" and contains(@class, "btn-success")]')).click();
        }).then(function () { // wait a bit for account to get saved
            return browser.sleep(3000);
        }).then(function () {
            return browser.findElement(by.xpath('//div[contains(@class, "alert")]')).getText();
        }).then(function (alertText) {
            if (!alertText.includes('Your new accounts have been stored')) return done(new Error('Wrong success message'));

            return browser.findElement(by.xpath('//a[text()="Skip"]')).click(); // skip intro
        }).then(function () {
            done();
        });
    }

    function checkAccount(done) {
        browser.get('https://' + app.fqdn + '/accounts/asset').then(function () {
            return browser.wait(until.elementLocated(by.xpath('//a[text()="Cloudron Bank"]')), TIMEOUT);
        }).then(function () {
            done();
        });
    }

    function importData(done) {
        const testFilePath = `${path.resolve(__dirname, '.')}/test_import_data.csv`;

        browser.get('https://' + app.fqdn).then(function () {
            return browser.findElement(by.id('transaction-menu')).click();
        }).then(function () {
            // Weird xpath below because the ID used on the parent isn't unique, for now this will have to do
            return browser.findElement(by.xpath('//*[@class="sidebar"]/ul/li[7]/ul/li[1]/a')).click();
        }).then(function () {
            return browser.findElement(by.xpath('//*[@class="box-body"]/*[@class="row"]/div[1]/a')).click();
        }).then(function () {
            // We are on 'Import a file' page
            return browser.findElement(by.id('ffInput_import_file')).sendKeys(testFilePath);
        }).then(function() {
            return browser.findElement(by.xpath('//*[@class="content"]/form//button[@type="submit"]')).click();
        }).then(function() {
            return browser.findElement(by.id('ffInput_has_headers')).click();
        }).then(function() {
            return browser.findElement(by.id('ffInput_date_format')).sendKeys('Y-m-d\TG:i:sP'); // ISO 8601 date
        }).then(function() {
            return browser.findElement(by.xpath('//*[@class="content"]/form//button[@type="submit"]')).click();
        }).then(function() {
            // Let's line up our columns with local data types
            return browser.findElement(by.xpath('//select[@name="role[7]"]')).sendKeys('amount');
        }).then(function() {
            // Let's line up our columns with local data types
            return browser.findElement(by.xpath('//select[@name="role[9]"]')).sendKeys('currency-code');
        }).then(function() {
            // Let's line up our columns with local data types
            return browser.findElement(by.xpath('//select[@name="role[11]"]')).sendKeys('description');
        }).then(function() {
            // Let's line up our columns with local data types
            return browser.findElement(by.xpath('//select[@name="role[12]"]')).sendKeys('date');
        }).then(function() {
            // Let's line up our columns with local data types
            return browser.findElement(by.xpath('//select[@name="role[13]"]')).sendKeys('account-name');
        }).then(function() {
            // Let's line up our columns with local data types
            return browser.findElement(by.xpath('//select[@name="role[14]"]')).sendKeys('account-iban');
        }).then(function() {
            // Let's line up our columns with local data types
            return browser.findElement(by.xpath('//select[@name="role[20]"]')).sendKeys('category-name');
        }).then(function() {
            // Let's line up our columns with local data types
            return browser.findElement(by.xpath('//select[@name="role[21]"]')).sendKeys('budget-name');
        }).then(function() {
            return browser.findElement(by.xpath('//*[@class="content"]/form//button[@type="submit"]')).click();
        }).then(function () {
            // Sleep for a while while the import processes
            return browser.sleep(15000);
        }).then(function() {
            return browser.findElement(by.xpath('//h3[@class="box-title"]')).getText();
        }).then(function(titleText) {
            if (!titleText.includes("Import finished")) {
                return Promise.reject(new Error('Wrong success message'));
            }
        }).then(function() {
            done();
        });
    }

    xit('build app', function () {
         execSync('cloudron build', { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('install app', function () {
         execSync('cloudron install --location ' + LOCATION, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('can get app information', function () {
        var inspect = JSON.parse(execSync('cloudron inspect'));

        app = inspect.apps.filter(function (a) { return a.location === LOCATION; })[0];

        expect(app).to.be.an('object');
    });

    it('can get the main page', function (done) {
        superagent.get('https://' + app.fqdn).end(function (error, result) {
            expect(error).to.be(null);
            expect(result.status).to.eql(200);

            done();
        });
    });

    it('cannot login as admin', cannotAdminLogin);
    it('can login', userLogin);
    it('can initialize when user logs in for the first time', initializeAccount);

    xit('can import data properly', importData);

    it('can restart app', function (done) {
        execSync('cloudron restart --app ' + app.id);
        done();
    });

    it('has the bank account', checkAccount);

    it('backup app', function () {
        execSync('cloudron backup create --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('has the bank account', checkAccount);

    it('restore app', function () {
        const backups = JSON.parse(execSync('cloudron backup list --raw'));
        execSync('cloudron uninstall --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
        execSync('cloudron install --location ' + LOCATION, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION; })[0];
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('has the bank account', checkAccount);

    it('uninstall app', function () {
        execSync('cloudron uninstall --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    // test update
    it('can install previous version from appstore', function () {
        execSync('cloudron install --appstore-id org.fireflyiii.cloudronapp --location ' + LOCATION, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION; })[0];
        expect(app).to.be.an('object');
    });
    it('can login', userLogin);
    it('can initialize when user logs in for the first time', initializeAccount);

    it('can update', function () {
        execSync('cloudron update --app ' + LOCATION, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    xit('can login', userLogin);
    it('has the bank account', checkAccount);

    it('uninstall app', function () {
        execSync('cloudron uninstall --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });
});
