This app packages Firefly III <upstream>5.4.6</upstream>

A free and open source personal finance manager

"Firefly III" is a self-hosted financial manager. It can help you keep track of expenses, income, budgets and everything in between. It supports credit cards, shared household accounts and savings accounts. It’s pretty fancy. You should use it to save and organise money. 

### Features

* Full financial management
* JSON REST API
* Reports in many flavours
* Import data from other sources

